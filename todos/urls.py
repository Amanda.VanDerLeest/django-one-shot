from django.urls import path
from todos.views import TodoListView, TodoListCreateView, TodoListDetailView, TodoListDeleteView, TodoListUpdateView, ItemListView, ItemCreateView, ItemDetailView, ItemDeleteView, ItemUpdateView

urlpatterns = [
    path ("",TodoListView.as_view(), name= "todo_list"),
    path ("new/",TodoListCreateView.as_view(), name = "todo_list_new"),
    path ("<int:pk>/", TodoListDetailView.as_view(), name = "todo_list_detail"),
    path ("<int:pk>/delete/", TodoListDeleteView.as_view(), name = "todo_list_delete"),
    path ("<int:pk>/edit/", TodoListUpdateView.as_view(), name = "todo_list_edit"),
    path ("",ItemListView.as_view(), name = "item_list"),
    path ("new/", ItemCreateView.as_view(), name = "item_new"),
    path ("<int:pk>/", ItemDetailView.as_view(), name = "item_detail"),
    path ("<int:pk>/delete/", ItemDeleteView.as_view(), name = "item_delete"),
    path ("<int:pk>/edit/", ItemUpdateView.as_view(), name = "item_edit")
]
