from re import template
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from todos.models import TodoList, TodoItem

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

    def get_queryset(self):
        return TodoList.objects.filter()
    pass

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.hmtl"
    pass

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    pass

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    pass

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    pass

class ItemListView(ListView):
    model = TodoItem
    template_name = "items/list.html"
    pass

class ItemDetailView(DetailView):
    model = TodoItem
    template_name = "items/detail.html"
    pass

class ItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/new.html"
    pass

class ItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/edit.html"
    pass

class ItemDeleteView(DeleteView):
    model = TodoItem
    template_name = "items/delete.html"
    pass